package first.mymapzz.com.mydaggermvp.ui.Login.mvp;

import android.util.Log;
import android.view.View;

public interface LoginMvp {

    interface View {
        void loginSuccess(String message);

        void loginFail(String message);
    }

    interface Presenter {
        void setView(View view);

        void onLogin(String username, String pwd);
    }

    interface Interactor {
        interface LoginCallBack {
            void onSuccess(String message);

            void onFail(String message);
        }

        void AuthUser(String usename, String psd, LoginCallBack loginCallBack);
    }
}
