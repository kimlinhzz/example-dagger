package first.mymapzz.com.mydaggermvp.ui.Login.mvp;

import android.view.View;

import javax.inject.Inject;


public class LoginPresenter implements LoginMvp.Presenter {
    private LoginMvp.Interactor interactor;
    private  LoginMvp.View view;

    @Inject
    public LoginPresenter(LoginMvp.Interactor interactor){
        this.interactor = interactor;
    }

    @Override
    public void setView(LoginMvp.View view) {
        this.view = view;
    }

    @Override
    public void onLogin(String username, String pwd) {
        interactor.AuthUser(username, pwd, new LoginMvp.Interactor.LoginCallBack() {
            @Override
            public void onSuccess(String message) {
                view.loginSuccess(message);
            }

            @Override
            public void onFail(String message) {
                view.loginFail(message);
            }
        });
    }
}
