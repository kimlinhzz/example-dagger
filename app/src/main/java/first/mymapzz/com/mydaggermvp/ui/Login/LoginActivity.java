package first.mymapzz.com.mydaggermvp.ui.Login;

import androidx.appcompat.app.AppCompatActivity;
import first.mymapzz.com.mydaggermvp.R;
import first.mymapzz.com.mydaggermvp.dagger.MyApplication;
import first.mymapzz.com.mydaggermvp.ui.Login.mvp.LoginMvp;
import first.mymapzz.com.mydaggermvp.ui.Login.mvp.LoginPresenter;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

public class LoginActivity extends AppCompatActivity implements LoginMvp.View, View.OnClickListener {

    @Inject
    LoginPresenter loginPresenter;

    EditText edUserName, edPss;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        registerView();
        ((MyApplication) getApplication())
                .getAppComponent()
                .inject(this);

        loginPresenter.setView(this);
    }

    void registerView() {
        btnLogin = findViewById(R.id.btn_login);
        edPss = findViewById(R.id.ed_pss);
        edUserName = findViewById(R.id.ed_name);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void loginSuccess(String message) {
        Toast.makeText(this, "Login Succes", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginFail(String message) {
        Toast.makeText(this, "Login Fail", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        loginPresenter.onLogin(edUserName.getText().toString(), edPss.getText().toString());
    }
}
