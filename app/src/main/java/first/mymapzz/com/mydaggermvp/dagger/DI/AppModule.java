package first.mymapzz.com.mydaggermvp.dagger.DI;


import dagger.Module;
import dagger.Provides;
import first.mymapzz.com.mydaggermvp.ui.Login.mvp.Interactor;
import first.mymapzz.com.mydaggermvp.ui.Login.mvp.LoginMvp;
import first.mymapzz.com.mydaggermvp.ui.Login.mvp.LoginPresenter;

@Module
public class AppModule {

    @Provides
     LoginPresenter provideLoginPresenter(LoginMvp.Interactor interactor){
        return new LoginPresenter(interactor);
    }

    @Provides
    LoginMvp.Interactor provideLoginInteractor(){
        return new Interactor();
    }
}
