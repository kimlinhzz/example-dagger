package first.mymapzz.com.mydaggermvp.dagger;

import android.app.Application;

import first.mymapzz.com.mydaggermvp.dagger.DI.AppComponent;
import first.mymapzz.com.mydaggermvp.dagger.DI.AppModule;
import first.mymapzz.com.mydaggermvp.dagger.DI.DaggerAppComponent;

public class MyApplication extends Application {
    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = createAppComponent();
    }

    private AppComponent createAppComponent(){
        return DaggerAppComponent.builder().appModule(new AppModule()).build();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
