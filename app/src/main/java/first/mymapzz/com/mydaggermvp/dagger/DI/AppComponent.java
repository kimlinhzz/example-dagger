package first.mymapzz.com.mydaggermvp.dagger.DI;

import dagger.Component;
import first.mymapzz.com.mydaggermvp.ui.Login.LoginActivity;
import first.mymapzz.com.mydaggermvp.ui.Login.mvp.LoginMvp;

@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(LoginActivity loginActivity);
    LoginMvp.Interactor getLoginInteractor();
}
